<?php

use App\Models\item;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('comments', function (Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->string("email")->nullable();
             $table->string("phone");
            $table->text("comment");
            $table->text("file")->nullable();
            $table->boolean("is_recomandation")->default(false)->nullable();
            $table->boolean("is_seen")->default(false)->nullable();
            $table->foreignIdFor(item::class,"item_id");
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('comments');
    }
};
