<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class item extends Model
{
    use HasFactory;
    protected $fillable = [
        'user_id',
        'category_id',
        'img',
        'number',
        'description',
        'name',
        'spesfication_number'
    ];
    
    public function category(): BelongsTo
    {
        return $this->belongsTo(category::class,'category_id');
    }
}
