    <!-- Day Selection Modal -->
    <div class="modal" id="{{$id}}" tabindex="-1" role="dialog">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">{{$haeder}}  </h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <form id="daySelectionForm" method="POST" action="{{$route}}">
                        @csrf
                        <input type="hidden" name="user_id" value="{{ Auth::id() }}">
                        <div class="form-group">
                            <label for="day">{{$lable}}</label>
                            <input type="text" name="{{$name}}">
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">اغلاق</button>
                            <button type="submit" class="btn btn-primary" id="submitDay">حفظ</button>
                        </div>
                    </form>
                </div>

            </div>
        </div>
    </div>