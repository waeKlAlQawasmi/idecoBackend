<!DOCTYPE html>
<x-guest-layout>
    <style>
        .slick-slide img{
            width: 215px;
            height: 35vh;
        }
        .category__tittle{
            margin: 0 0;
            width: 100%;
        }
    </style>
   <!-- Banner Starts Here -->
   <section class="section banner-two overflow-hidden">
    <div class="container">
        <div class="row">
            <div class="col-lg-7 mb-4 mb-lg-0 order-2 order-lg-0 d-flex align-items-center">
                <div class="banner-two-start">
                    <h1 class="font-title--lg">قسم المواصفات و المقاييس </h1>
                    <p>
                        الموقع الرسمي لقسم المواصفات و المقاييس شركة كهرباء اربد لعرض المواصفات القياسية للمواد التي تحتاجها الشركة </p>
                    <form action="#">
                        <div class="banner-input" style=" direction: ltr;">
                            <div class="main-input">
                                <input type="text" placeholder="ابحث في المواصفات " />
                                <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-search">
                                        <circle cx="11" cy="11" r="8"></circle>
                                        <line x1="21" y1="21" x2="16.65" y2="16.65"></line>
                                    </svg>
                            </div>
                            <div class="search-button">
                                <button class="button button-lg button--primary" type="button">
                                        ابحث
                                    </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
            <div class="col-lg-5 order-1 order-lg-0">
                <div class="banner-two-end">
                    <div class="image">
                        <!-- <img src="dist/images/banner/banner-image-02.png" alt="Instructor" class="img-fluid" /> -->
                    </div>
                    <div class="image-shapes">
                        <img src="dist/images/shape/dots/dots-img-01.png" alt="shape" class="img-fluid shape01" />
                        <img src="dist/images/shape/rec07.png" alt="shape" class="img-fluid shape02" />
                        <img src="dist/images/shape/rec06.png" alt="shape" class="img-fluid shape03" />
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<!-- Intro Featured Starts Here -->

<div class=" home-top-feature">
    <div class="container">
        <div class="row feature">
            <div class="col-lg-4 col-md-6">
                <div class="cardFeature cardFeature--center">
                    <div class="cardFeature__icon cardFeature__icon--bg-g">
                        <svg width="32" height="32" viewBox="0 0 32 32" fill="none" xmlns="http://www.w3.org/2000/svg">
                                <path
                                    d="M2 4H10.4C11.8852 4 13.3096 4.5619 14.3598 5.5621C15.41 6.56229 16 7.91885 16 9.33333V28C16 26.9391 15.5575 25.9217 14.7699 25.1716C13.9822 24.4214 12.9139 24 11.8 24H2V4Z"
                                    stroke="currentColor"
                                    stroke-width="2.5"
                                    stroke-linecap="round"
                                    stroke-linejoin="round"
                                />
                                <path
                                    d="M30 4H21.6C20.1148 4 18.6904 4.5619 17.6402 5.5621C16.59 6.56229 16 7.91885 16 9.33333V28C16 26.9391 16.4425 25.9217 17.2302 25.1716C18.0178 24.4214 19.0861 24 20.2 24H30V4Z"
                                    stroke="currentColor"
                                    stroke-width="2.5"
                                    stroke-linecap="round"
                                    stroke-linejoin="round"
                                />
                            </svg>
                    </div>
                    <h5 class="font-title--xs">+1000 مواصفة</h5>
                    <p class="font-para--lg">
                        عدد ضخم من المواصفات الشركة تضم مكتبة واسعة و مصفة </p>
                </div>
            </div>

            <div class="col-lg-4 col-md-6">
                <div class="cardFeature cardFeature--center">
                    <div class="cardFeature__icon cardFeature__icon--bg-b">
                        <svg width="33" height="32" viewBox="0 0 33 32" fill="none" xmlns="http://www.w3.org/2000/svg">
                                <path
                                    d="M22.3854 14.2241C24.8741 14.2241 26.8914 12.2068 26.8914 9.71806C26.8914 7.23066 24.8741 5.21204 22.3854 5.21204"
                                    stroke="currentColor"
                                    stroke-width="2.5"
                                    stroke-linecap="round"
                                    stroke-linejoin="round"
                                />
                                <path
                                    d="M24.4575 19.121C25.2009 19.1715 25.939 19.2781 26.6674 19.4394C27.6774 19.6403 28.8938 20.0544 29.3257 20.9606C29.6017 21.5414 29.6017 22.2179 29.3257 22.7988C28.8951 23.7049 27.6774 24.119 26.6674 24.3268"
                                    stroke="currentColor"
                                    stroke-width="2.5"
                                    stroke-linecap="round"
                                    stroke-linejoin="round"
                                />
                                <path
                                    fill-rule="evenodd"
                                    clip-rule="evenodd"
                                    d="M13.5993 20.0912C18.6424 20.0912 22.9503 20.8552 22.9503 23.907C22.9503 26.9602 18.6697 27.7502 13.5993 27.7502C8.55612 27.7502 4.24963 26.9876 4.24963 23.9344C4.24963 20.8811 8.52879 20.0912 13.5993 20.0912Z"
                                    stroke="currentColor"
                                    stroke-width="2.5"
                                    stroke-linecap="round"
                                    stroke-linejoin="round"
                                />
                                <path
                                    fill-rule="evenodd"
                                    clip-rule="evenodd"
                                    d="M13.5992 15.7349C10.2727 15.7349 7.6076 13.0684 7.6076 9.74188C7.6076 6.41669 10.2727 3.75024 13.5992 3.75024C16.9258 3.75024 19.5922 6.41669 19.5922 9.74188C19.5922 13.0684 16.9258 15.7349 13.5992 15.7349Z"
                                    stroke="currentColor"
                                    stroke-width="2.5"
                                    stroke-linecap="round"
                                    stroke-linejoin="round"
                                />
                            </svg>
                    </div>
                    <h5 class="font-title--xs"> قوائم الموردين</h5>
                    <p class="font-para--lg">
                        انضم لقوائم موردين الشركة حسب المواد التي ترغب بها </p>
                </div>
            </div>

            <div class="col-lg-4 col-md-6">
                <div class="cardFeature cardFeature--center">
                    <div class="cardFeature__icon cardFeature__icon--bg-r">
                        <svg width="27" height="27" viewBox="0 0 27 27" fill="none" xmlns="http://www.w3.org/2000/svg">
                                <path fill-rule="evenodd" clip-rule="evenodd" d="M25.2502 13.2495C25.2502 19.8774 19.8781 25.2495 13.2502 25.2495C6.62235 25.2495 1.25024 19.8774 1.25024 13.2495C1.25024 6.62162 6.62235 1.24951 13.2502 1.24951C19.8781 1.24951 25.2502 6.62162 25.2502 13.2495Z" stroke="currentColor" stroke-width="2.5" stroke-linecap="round" stroke-linejoin="round"/>
                                <path d="M17.7021 17.0667L12.8113 14.1491V7.86108" stroke="currentColor" stroke-width="2.5" stroke-linecap="round" stroke-linejoin="round"/>
                            </svg>
                    </div>
                    <h5 class="font-title--xs">اقترح او قدم ملاحظات </h5>
                    <p class="font-para--lg">
                        يمكنك تقديم ملاحظات او توصيات لاي من مواصفات القسم </p>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- Featured Categories Ends Here -->
<section class="section featured-categories">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <h2 class="text-center font-title--md">تصنيفات مواد القسم</h2>
            </div>
        </div>

        <div class="categories categories__slider" style="direction: ltr;padding: 0px;">
            <div class="category">
                <div class="category__img">
                    <a href="#"><img src="https://media.istockphoto.com/id/499367414/photo/power-transformer.jpg?s=612x612&w=0&k=20&c=JPE6xPauBRCkoJCIXJ78yJFJbmd_wGBtHfbFMFkeWH0=" alt="images" class="img-fluid" /></a>
                </div>
                <div class="category__tittle">
                    <h6><a href="#">محولات</a></h6>
                    <span>+10 مواصفة</span>
                </div>
            </div>
            <div class="category">
                <div class="category__img">
                    <a href="#"><img src="https://5.imimg.com/data5/SELLER/Default/2021/8/WU/AW/DP/98125190/safety-equipment-kit.jpg" alt="images" class="img-fluid" /></a>
                </div>
                <div class="category__tittle">
                    <h6><a href="#">معدات سلامة عامة</a></h6>
                    <span>+100 مواصفة </span>
                </div>
            </div>
            <div class="category">
                <div class="category__img">
                    <a href="#"><img src="https://www.emworks.com/blog/storage/uploads/2017/06/cross-section-of-a-3-phase-submarine-cable.jpg" alt="images" class="img-fluid" /></a>
                </div>
                <div class="category__tittle">
                    <h6><a href="#">كوابل</a></h6>
                    <span>+10 مواصفة</span>
                </div>
            </div>
            <div class="category">
                <div class="category__img">
                    <a href="#"><img src="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcR_pVnyto5EYNCUwA-kY4WAwm9_qhfVOR5aAw&usqp=CAU" alt="images" class="img-fluid" /></a>
                </div>
                <div class="category__tittle">
                    <h6><a href="#"> مقادح</a></h6>
                    <span>+10 مواصفة </span>
                </div>
            </div>
            <div class="category">
                <div class="category__img">
                    <a href="#"><img  height="200"  width="300" src="data:image/jpeg;base64,/9j/4AAQSkZJRgABAQAAAQABAAD/2wCEAAoHCBQSFBgUFBQYGBgaGRgZGxgZGhkYGhkYGRgZGRkZGRgbIC0kGyApHhgYJTclKS4wNDQ0GiM5PzkyPi0yNDABCwsLEA8QHhISHjUpIyk2MjI2ODs7MjI1OzUyMjIyMjA1MjIyMjUyMjIyMjAyMjIwMjIyMjIyMjIyMjIyMjIyMv/AABEIAQMAwgMBIgACEQEDEQH/xAAbAAABBQEBAAAAAAAAAAAAAAABAAIEBQYDB//EAEUQAAIBAgQEAwUECAQGAQUBAAECEQADBBIhMQUiQVFhcYEGEzKRoSNCcsEUM1JigqKx0ZKywvAVU3Oz0uEkY6O04vEH/8QAGgEAAwEBAQEAAAAAAAAAAAAAAAEDAgQFBv/EACcRAAICAgICAgICAwEAAAAAAAABAhEDIRIxBEEyUSJhE4GRodEU/9oADAMBAAIRAxEAPwCwwvELd05VaHG6MCjjzRoMeO1Sq5YvB27oAuIrAaiRqp7q26nxFRP0S9b/AFV3Ov7F6W/w3RzD+IPX0G0eTplhFKKgf8UVdL6NZ/eaGt+l1eUD8WU+FT1YEAggg7Eag+RpppiaaFFKjSpgNijRoUgFTYp1KgBlCnU13C6sQNQJOmrEADzJIHrQAqzj4a3OS4/u3R2VHkIXRodACfjguFA8GrR1CxnDUuXEu/fSApkxGaSCvXTMP4jWZx5I1CVMo7mBuvq93NbV2KMZQsV5YLprbWc0Nr9RRw+NxFuBcYodouoHU/hvWyJ9VJrThQNBpUd7aqYgZG0KxKhiRGnQHWfGO5nH8fH4s3/LfaIVrF3yJ90jjvbuzPo6rB8JqVg8Ut0EiQVMMjCGRuzD89juJpi8MtohS2XtySSUYzJ3+KR9KzPEXxmEc3CQy/Dn5JK/dVh/6iiU3BW1YKEZaRr6BFU+H9oUflyMHgcpgA+KmTKz1FdbXE2N5LbAQ6uQRuCkGD3kE/KkvIg5KKfYv4pJN0WUUop0UIroJjYpUSKUUAMpU6lQBYUqJpVkARVe/CUBLWmayx1OQgKTvLWyCh84nxqxpUNWCbRXLcxCGHRbi/tWzkYa9bbmNuzz4VwucX1LrlNpCFuTmW4ksVLlCPgUhZ8Gnprb1mcXYtXL92y9stdc50YTCL7tEDMQRADE1LJyVcWUhxfaNErqSQCCVIBAOxIBAPoQfWnVnsBh3tO9r3rK7lsjtDK5A+F0b76iDCkZlI3g1NW/irY+0tJc35rRKmOnI2pPlTjkdbQpQ3plnSqvs8YturNDqFMNKNynSc0TG/Wp+YbSJrakn0YcWuxhuDPk1nLm8ImN++tQuL31S20gMVCuF0nkdSDB8RI8qbxYXZQowQB5dtyUglkAjckLr0386bGY9XDWwmXlcmCWLHITqSJJ0GpnaozypXH2Wji6kalZjUQeomY9etKq67xmygAz5mgciczT200B86q8Zx64QQmS2BoWYhmBjsdAfOnLyIR7ZlYpS6RpGIAkmB3NVWL4zZgqpLk/8vXyIf4Z66E1mTjPenL9piXHRQSo8ey+lS14fiIm49vDKekh3Ph2/Oud+TOS/Fa+yywJdsmvxi4VlyloDQknfxBbTXt59qqsRxW0czoj3yNC8NlGuxc7a+FNOHtC4CJxG4l1hcxiOdpbv0GsRT8ZxCB7u7cVLcTkUbQRoFEsRsRPY1yyk5fJ2XjFR6Rww3CLl0oblxLGXMUQAtdCk75dCBrANWnBsLYt3ICu99QSWcsx10zL0gg9dRrUTDpduZTh8Oyw0+8unIpBGoyDVge4PQU3i/Ari22u3L4zkqCqLkSCQDmIgsBvr2rcYyq4xr9sTcbpv+jRYHHm5dvWzlhMhBGujqZB1OoZT6HwqwionDcBZtKPcoFDAGdyw3EsdTv9amGvSxKSilJ2zgyNOT49DDQp1CqmARSpUqQFiaFQhxW31zoez2rqfVkg+ho/8Wsf8xfr/Ss8l9muL+iZSqEeK2h+234bV1v8qGivEAdrd4+dtkPyeDRyQ+LJlcnsKWDxzLIB0mDEifQVHPEANTbvDytu/wBEk10sY+3cOVXGaJyGVcDuUYBh8qLQU0NxmEVwQyhgYlSYmNVZW+646ER5iARwt4W6F+zxDEESPeqLkAjQSCjHzYk07iHFUssqPILg5GiVzAEhWI2JjTvUY8W5ctq2zQAAz8q6CJ7n6VKeWCe2UjGbWkdMRg8RcUo74dlIgg2bn5XtPSqHHcJuWzmS9bU6Qi51UfgUlsvlNcuIcZzhkuXmkkqEtAEnoII6nsTOtdMNh8Uygi2mHWBNy8ZbbWFOq+TaVyZMqnqKs6IwcdyY/E8VvW0CXFVkaEV+ZTmIPxCZPbN4SQJqmxGJtA81xnY6ZLYgHsC33vnXLHYUuHzYg3XVkCkSEy/eIB0PbTaPEVVPhWSGUmZBB8RtXLLk3ssuKWjRhLq2vegJh7bKGUznuupjLlXYzI3iKbwyzZK57ivfuEsRnYpbRekydTGpABHTprWcP4RfuQbdtnJIBZtFVAdFzNpt0H9qvMHwm4bvumdc2UG4bY0toZ2dtTcOgEiBvrBqqhLTiv8AJlyW7Y9+MgIR7xLSqWXIgCfDpIIlvUAVCwuGu3TNu07yfjucifiE6tWtwHAsPYj3dtZH3m5m9C0x6RVia6l4re5s53nS+KMhf4eEA/TMVkkEi3ZBWVG4GhZx6VEs+5t28vuclxHVmJWGCK6sJLMTtDECBptWk9ouHtftFRGnNsC3LrCnpO3Wqb2isLmS4JKumSe/LGviVP8AWieNQukOORy7ZrIqo9pknDuSYA/Pl/OmcF4uXVluAhkyksIaVcFkJCgawCDAjlNTb+LtkRm10Ox7/wC9Ks8kHHb7IqElLSJFi3lRVOsKB8hFPrnh7y3EV0MqwkHbTyNdKtGq0TfY0igadQpmRtKjSpgcmtXE/V4rN2Vwjk+qgN9DT0xt1LYNxVd5AKJIaCegeCaszQYTodRUuFdMpyvtFC/tRaRylxHQgkEGJERMiY+RNWacRssudbqFepDqYnoddD4U69gLbiGtqfCIH0rC4jB4l3JS04ZTz3LvICRGhYtzqfCemtRnKcP3ZaEIy/Rpcd7T2kB92puEdRyrv3In6VR8S45n/X3EQAghEVXYHoZMlT46Vxbhlu2xGMvsfhOSzAQ5pGVmOoMgiCB0gzoJIxdvD8tmxZsrsXchnM92Y+O0muWc5S+Tr9ItGEY9KyvweNe+xWyg1Vsz3mMRInRZ1ABMTMZj0NTf0axqMTinvka5U5bZB7ld9dJnt3qHma60Wfe3xH3FyW1cHWCwgegocIwJxVxrZYWyAHDKM5ZWUQAWOnxTI+VThF+l/k3J/bO+LxlpkNtLNq3b3zEANI2Ic6z4iTXEsbyl1z3FQDK7BwnaM7KQNdK1XD+AYW2Qfd5373TneR1AOnqBV1HSupePJ/JkHmiukYX/AIReFv31x7eUDlRJaQ0al21BGum1VlyyAM7bbDuzESI+lbb2mZ1w7MoBCxK7bkKCD4EgkdQCOtYc4a7cIbK2UDlXUQPI9TE1HKuLUaorCXJWXfBeLmzYcHmYMotp1Z7moX/EW9FrRcG4ebNvnOa45L3G3l21IHgNvmetZrhCBHW9cghEUW0DGAYgu0jRoEafkKsb/HbhYKAEWCc5GmkTzNpOs7VvHnjFbd0TyY5N6XZo4qO2Ktj7wPlr/Sss/FUcwHa637KK1z5RoPnRz3mGltUA63bgBH8CS3pRLzG/igXjL2y8xHFVX4RPidB/f+lZDivFXj3ATRTmBMBQNWXXfbMoEjbwqxGGzH7S+/lbQIP8bSx61HKYa24JCkgNJbmkyuU/aFux1EVCWaUu2VjjiukVN+4jsotZmdliVUgzElZ20AOoJ0B6CpOGu37VvS2oALAu5JgyRzbAag66/nUo8Rt3DKW3uMoOqqz5QJJljsN9NvClZt3bzvZCome2twlzmlDyZkySOgGtY4yfSZu0uydgeLGxhlATOUDE80aFmMDQzEgeh8q0li4LiK42ZVYeTAEf1rNcG4ILlsZ7rwpKFEKhQQfhLazuD61prFlURUUQqqFA3gKIGp8BXpeNz43Lr0cWbjf49+x1CnGhXQc42KVOilQBPilRNCkaFWa9quI3MO1l0CkBzuXnMVMSqkAgjNv1FaWqf2ow4fDsYBKFWE+cH6E1jL8XRvH8lZVY3gV2+8++RLLwQVQs53YA5jA3MMPlXPh/CrFrFtauIbrEBke6c52lhHwnXWY6incE9obYt+6uzmTRQoYkpuDOwIOm42FccTxoXListvK9vUO0c6EMMrIBHWdDuBttXHJ4o01339nSubtPo2KoFEAAAbAaAelYzFXRhMbmiFLCegCPCn0UQf4aZjfaBjyteynsmh30HLzVVuLpuh/duVCtpdBRWUrEgvqTmbTQk9NiQp+QpVxXQQwtXyfZtb3HbEQJeey8p9WgHpVe3HXki2oA8SXjyOkeWorIrhbq2uS4ucMYQKx25fjaBBAkAA77insllkzFbjQBIuXJAOsnkCwNv9zU5eRkfuikcMF+yw4lx9zby++VnDagQQVB3hBA6b7GoN32lBEW0J8TCj5Cf60k4iHQ2FK5SjDKigaZTBbL8XTU1GTDqFOg6VKnLdm9R0NTiTghQzAtJK2wqxJMZbjZmHXpXB8TDliJ0Ah4uQZ1YlhBPoB4VacM4YHz3CCQipKpq5VngBAATJGbWPu1cY7gWHJXC2LYzuAz3Hl3tpprzfCx7CN+kg1SGFtWjMsiWmUXDeIYl8y21dwYHKrMFI7BeVdPAVcYbheOcfCqDvcYT5wmY+lbHC4ZbVtLaaKihR10AjU967EV1x8SPcnZzvyX1FGYt+y7t+txLn922qp6FmzT8hUrBez+GQsfdK/NALy/w7mHkA5sw07VcXnyiRvso7sdvTqfAGilvKAOw36nxPiatHFBdIlLLNrbAqgCAIA6DQfKqzE8OcEmwyoXIzBhIABLFkgSCTErsZJ0Mzamgao0mqMKTRA4Qii2CFAJZ80SZdWKMZOp1WptROFDkYdr2I//ACLhH0NTDRFUqCXbG0KdQpmBtKjFKgCeaFONCkaKn2lxT2bBe2SsMMzATCwZOxjWNaxTWMViTyW7twRo7BkgnfK7x000r0ulUMmHm7bLQy8VSR5T+g3LboLlxUynKWXnJDMZaScjQAD8WgPTapmKwtm2TmD3JVGDO5WFaQTkSBpvvHnFXNz2Pd2Oe6oSZEKzMw35gWgfWuz+ydoXLZdne2oYPmdgToMi8sZQCWMyOx8eT/zz+jp/lj9mcs4pLDRYGV2GXlLM7dSAJJ6bU9eHYq7cJW2wLKX5+SQIU6Hmk8sadPOtRwi9bt3LwS0lqysjONJZBzTp2DGOmXuTVrgbbHNdcQ7xyndEWciHx1ZiOhdh0Fbx+MmtsxPM10jOYP2Scw1y+ANDFtfX42Oo/hrpZ9kgqXB7w5i5KMSScmVeV4Ag5sxkeHlWmHIY+6Tp4MenkTt46dQK610LBBeiDzS+zN21a1hntXECnI4BUKASQYzBRHrWSZFClmYACAB1ZiNB5ROvhXoXGcP7yy8EqwRyrDcMFJHmK87/AEQuQXIMDKBqQACe+prlzQcaXr0dGKSlss+HXAmGa4eZmuNbCDQvnVCgHirKCD2Ld60/s9g/d287HNcuc1xjvO2Xwy6iO81U+y3DkZ/enXIIRegLDVvOFA//AIK0uGYS6j7rn+cB/wDXHpXThjpSZLLLtI7RSrlddgQB1I3BM9xMiIAJ6/3dfuZRPXQAd2JgD5xXRZz0MUZnJ6LoPxEcx+Wn+KulC2mVQN+57k6k+pJPrTjQgY2lRpppgRMANHH/ANR/qxP51JNRsEdbo7XD9URv9VSaUegl2A0qJoGmZBSoxSoAnGlRNKsmgUKNKgAGhFGjTApsdwuGN5JZlAIt6ZDkIYBRHxRmAk/f6CrKzfVwCp3AI7kHUEdx4jSu1QsIgGe0QCEOZQdeR5KadIIdR4IKwlT0abtbJTKCIOoNc+ZehYdx8Q85389/A70xrLZmXOwIjk1IAJIBLfFJynZoHakUUfGg/F8Q9WIkeunjTUr2garR0DqdD21BBBjrodYrE8YCq2gjXoN5A7etbQ4e2QDkQ7Ecq7gypHqAZrG8ddZ2jqPEf7Irj8m7R04K4s7+zPErdtjbcke8KBTpGbUAHWdSwG0d40nQs4S+ZnntghQN2ttDR4kXU36L4GsfwfhRxLjmARDL9yCTAX5HXpWwv2ghtMPuuEYklmKupRQWOp5ylVwOXHfRjKo2QuM8Ve1kSFMOH+JdAQVywSDEA666zttUgv7y+gYrlUMyKDMtJUO38OaFjSSe0cOO4e5chEzGACYYiObSV2M69vhNVlvD+6xXvXZYzOTqAxzKw2J7tv8ASpvKozab9mlj5RTS9GsNA1BtcVtOYDx+IMF9HIynympxrrjNS2mc0ouPaBTadQrQiDgvjxH/AFV+X6PY/Oal1Hww+0u+Lqf/ALaD8qlUIcuxlCnUKZgFKlSoAsDQpxoVg0ChRpUwBSo0KABUPGnIyXNgDkb8DkAH0cJqdgWqYap8Zx+wsoczzIOUADX95yAfMTWJyjFbZuEW3pEbg+LuG5ibl24GygE6qYC5jMKT0nbTej7P439JZ2ZQVECSqzJ6TuRod/CqrCY63Z97mUn3gjRkygajmyRHxbrO+3ey9jY93cIEfaHvMZREg+Z6Vz4pp0k/stkg1bZcPg7c5iglSWBHQwRIjrBPzrEcaxKvLQQRJj1BrfuND5GvOniT4f2FLyUtf2Px29jOD8UNl0uD4GIVpn4CdTA6jceVb/HLntNl1OXMn4hzJ9QKwKYV7sLbUHUCTIA9R/7rY3uIGx7svAQZQ8BjoBDGNTGoI66UsWRRi0x5IOUk0RcdgL1xDfJlXJCKDy5RIQkePc1WHCKAJQCfAetb7Be6uYVRajIQ4WBAHMRt3BrJY/Qnx5vJtm+oNTg1JtmpWlRmMYhDgLoJnlgEwNtvP51pfZzFXLjZbgbkQ5XhgpDMsqxIhmGVSCOjeZNFiRzr5n/Kx/Ktrw4AWkgRyKdO5Ek/Oathj+TaJZJfiSKFOoV2HORLP6y5/Af5Y/KpNRbZ+2cfuWj82vD/AEipVJAxtKnU2mZBSoxSoAnmhSpVk0KhRpUDBSpUqBEHiBVytjOUa6VUNlJEZ0BWRsSGrN/omR2HSTE7gToK1yWw1+0CAQJbXur2mU+YKz6VTcVw7I7mI5yPQyRHpXNk+Wzox/Ez+LsA7ipfs21u3ceHg5dRPL3kjYRB3/Oq3iuIuIoKKX5gGH7sGY9Yq79kTzXDtIQ/5qlFXKijdRslcX4s1u1msxcYnpDCIkiAZkiToD8JrEXMQzsSVVQWzcpzcusgbdxW59obaOiqVEM4BMCSNZHcyCfkax3EsJ7tmWCYb4+hJOvTxFQyz/Krsrjj+N1RIsYxlVmw9tiVMxDnVoHwD4o0P+zXEYq4zTcds5EHpAI1EdPKncMxPuJuZZyuCdY7DbrvV5xfjK3LMKjK7KroHC/CdZDAkbTGszTTBo1HskoGCQDYM/8AnJ/OqbjduHPjr/f8jVr7FtOCEkk5338SG/OoXHlrWN7MyWjI49J85H1OX862fC/1KT+wPpWQxiyreRjzjStdwhpsofD8zXVi+TOfJ0S6BpxoV0kCIo+1bxRPo7/+VSK4t+tXxR/5WT/yrvSQMFKkaVMBtKnUqAJpoUVYMAQQQdiDIPkRRisjG0qdQigAUKdFCmA3A63iexRfUc5+jr8qHHrYMjuPqNRT+FavPe4/8rFB9FFLjg0J7Vx5vkjox/FmFxKTIIrpwe6+Zxat5nVS2UsqBug1J11O8aSK445jOg0J18BrUvgCQbjiZTI2mpK8wcR1OWYHcCspXKjd0rOyYe+EN7FMisWQKoiLa5tdZjXSd/hEntWcXZFDhmWWeRqJ1ZROniK0XtPZa5hyoGeWQgLIO++kyPGKw+JwipIMbwSoichiT8qh5EEpKiuKbcdkh7cI4Pg0+EqNqiWMexVFfXJKKeuSBC+kVHN1gGnMQViZkLtNOw6daExtHpHsM/8A8dgfvXHjzVUn6MPkaXHBoaquAcRtWMKWuXAri+xVNSXARAYUa/eOu1DiftBauSFV/Mhf/KmpRT2zLi2itvVo/Zt5w1vwDA+YdgfqKxuMx0CQPnEetWHsRYxDt703F9yC65AX1Y85yrGWMzzm35Y710YsicqWyM4Pjs2lCjSrsOUiOPtk/Bd+r2f7VIrjcH2iH91x88h/Ku9CBgoUaFMBUqVKgDu+DtklguVjqWQlGP4isZvIzQRLi7OHH74Ab/EkD+WpBtxsSPLb0B0HpQ1HY+Wn0/8AdYNWR2xLKCXttAEynOPRRzk+S1xscYw7sEW4oY7I8o3+FwG6Hp0qcXHXTz/vtRZQRB1HjqKB6FTa4rgba/AMn4CUEnqUHK3qDSZLg+F1YdnWCf41MD/CaLFR24Puv47n+d6dxo6GsjgPapvtUtW3a4j3SrZGZFJc/EqLnIXOOkntVXd4hjrmtxbpYgErbg7/ALSW9vIiuPNK2mlZ14o0mmScSNTVl7J/Fc8k/q1ZhLOIY8uGvk/vI6/MkVqPZTA37Wc3UC58uVMwLALMkxp95YG/lSxTbktMJxSi9l1hBlzW/wBiMv4GnJ6CGX+CsbxmzHvIBjMZjYakem1bPEjKVuDpyt+BiNfRgpnoM3esj7R4YFjpBBcgjSSzc2/SJpeZ6H43sz2GSQViSf7g11NoqYIio36N7y6lrNkDNBc6BR1PyrRY+7aYMFYTyxzBjogBlp5jI361BP7KNFO2HV/iE/Qj1FTuGez1q4rFncQ1sCG6O4U7jtUZHHerrgzxrOhe0m+kl1Zf8jD+KrY4wlLasnKUktE2z7HYRSCUd4/bdiPULANWmAtKhuIihVV1AUAAAe5tHQDzNToqNhvju/8AUX/s2q7YwjH4o5HOUu2dqUUYoVQyRr/x2/xMPmjH/TXeuGJHNb/6h/7VwVIpAxsUKdQpiBFKjSoFRN94PLz0/rRinkVza0IgSviukeMbfMVOzYYpptjy8tPnG9JLbKILZ/FgAfUqI+lHP3B9Nf6a/OKLHQ2D3nz/ALj+1AvG4P8AX+mtdAQdiD5a11tgbH50N0FFTgCnvLptsrK+RzlYEZ4ZH22MIk+JqZdtK4hlDDswB/rXHH4RM6Ow65CwJUgPGU5lgznCDf7xrnicNdRIsXJaRAukssdeYDOT5mkmaaOv6KB8Luvk2YeQV8wHoBUb3F1Hn3gcMTCtyZdAfiAafhOkDeob46/buN7z7RQgJXDpmZCNSWLHqOh1PTxi/wDHy19bXMto6+9ZShBykheYDL1GomsuaXZpRfomcY40ljIlxCvvGKc4lSsQdUkbsujRoTVXx0kdZiJPeQJnx3+dXXFcLaui3nXOueNCxkFHMch11ArP8atKoyoIQBAqkEEKqgAEHUGNNROlcvlXo6PHrZnfcG7dRQ2QlwpMTGYxtpMT4VbcS9mr1m2WV0eMx2cE5VkAKJ15THc6dqqcA8XUPZ1PyIr1K9bzAqdPEbgjUEeIMH0oxY4zTszkyOLVHluH4dib2H/SLQVlDMCqhi5CwCyrHN5DtT+E2sYEt3Vg2nuqWVgsk2GZycpEiBbfYgkiK9G4aOQ6ANnfMAI5i7MdPUHyIrnbtqzJps95x55mRv8AO3zqsfHSppmHmbtNE2o2HEPc8XVvTIij/Iadg/1ag6lZQnuUJSfXLPrQta3HPbIvqAW/o4rps5/s70KNKmIjYkfAezr9QV/1V1rli/hH47f/AHEFdqB+gUopUqYgUqNKgCwNKiaaalZsVKo93H2k+K4g83X+9cG41hgJN+2ANyXED1rPNfaNKD+mTmUHcA+dcFwwQlhcdRrILZ18/tJKjyIFR04rbufq3Q/vM2UegPM30B71ITCqxzsc56EmVBHVU+EHx38aFOMuhuLXZDuYi5clUCXUaQWUvbgEHVXIZXM9iCN6dh3LT71nDqdQCUUdiGt7gjXU7yOhAsq437RkOnxjTXQMvVW7eB6HwJBdBYzC37ZLJbZCV1ZUI0za5iB379abcQC4p6OCrdjAlZHXqNelRbvC7GIdb+SLqEidVYEj4bgUjNEyNesgwdazjuCxtu5bNi24BJMi7KwMumRydd9cv0BrEpUajFNjON+zNtjbNhUtNnAJQFQQBmGZV0aMvUdaj8dRgJcy2UAlRAJBIYgeddPaHieJshAEDgkQ2W4j54gjop0JMiPLTWt4lfz2+UMAWaUfVlVmOUNJJJlXBMmSJ61y+S1WjowX7M4tzKwae3z/ANxXq/6SoAJYQdiDIb8Pc+G9eR3rYYkHb5da9Tw9i2loXADogaZ6ZQ2gOg2GgEVTxm9k86WjnhyzIjCUVlUMT8Rn4WUfdmYk667CJqUiAXAAICJAHgzf/pTcOvu0Ft9gijN02ykE9Nuvz6B2GnO87jKnmACwPhOc11I52G1o7r3yv/iGUj5oT/FTMFquf9sl/wCE6J/IErlxD4kUHV8yHfRDBdtNjyhQe7ipwFaE+htKnUCK0ZI2MHJ5Mh+TqfyrtFc8b+rc9lY/ITXUisj9DaFONCtCBSp0UKAM97U+0zvay2Gy3AwMKoTQAyCR57GpOB4dZdEbEtcuXiisyPcd2BI1+yTYeYqZhvZa0jBmBJEEKHaARsSwgsfl5Vc27SoMqqFHYAAfIVxY4N7Z0znWkV1rhdsfBYt2x3yIbh+hA85PlXS5wi2ylYZZIzMDzuo+4XPMF8FI8IqwoVdRRPkykfgFtP1du2y7+7uKGXucjkFkOp3keVNs8Pw7HKoexcj4FdkYeKrJRh4gEVe1yxGHS4MrqCNx0IPdSNVPiKy4J+hqbXshraxNvVLiXR+zcGR/R10J8wK7Jxy2Tku22suf24ysf3XGhpsXbe03U7ae8X8n+h86k2hbvoQMrrsysNQezIdQfMVNxrp1/sopX2rJ6WUYhlYqw2ZYmOxnRhvoZ30g60cQ7sIuW84/bTw+9knOp8FzGqteG3LPNh3kdbVwkqfwOdVPgdPEVMwHGbdxvdsclwDW2+jeYHUeIqLlvfZVLWuiO1u2x0umQC2QhBcEA6hCmYGJ6VifaUZbhLOrGSBliSCAyDlkFgJBgxPnXpl7I4ysoYdmAI+RrAe2du2LjAKqiFy7ABsusRsNR9aWVuUaYQSUrRg9QdQa9K4S7XMPahZVEUN0zOgEL1MCFJ01JA/aFee3F5tSZnX5Vu+D4LDYfBJivds5YHOodua4zZEgZsq8wVJjTNJMA0sU3EeSPIsbdzOw5+VkzbZcwbYLOvKN9vjGgOoZYxKtme3DyzAlTyQsLq400y9JO+ld09nMxz3roUjUrbVURYM6u4LvHfMo6wKfw3hYw9kLbVArc+RgykFtd9Y0gERGldSyWzncNHN8MQrEnM5gzsJQ5kUDooPTxPepCsCARsRI8jtXJbpXRwViNRzCDsZ3jpJA2pYUjLAIIBIBGoy7rB68pWrJok0ztQo0KdiOGMH2b/gf/Ka60LiyCO4I+YoWWlVPdQfmKVh6DSo0q1YqGUqdSoAsjTSKz1z2qX7ttj+Jgv8AQGu/COMtecq6BJEpE6xuDO567DY1zqabLODLilRoVSzFAo0KNOwDtTLmCF055KOBAdeVgOxOzDwMinU9HIrElaNxdMjPjblnS8AV/wCagOX+NN08xI8RRxNlLyq8gkao6kEjyI3Hh+dSGuTVbcwGQlrBFskklI+zcnUkoPhJ/aXvJmp8NUzfM5YnjJw8LdRiSQFddVMmJJO2403896yntHxH35dgsQFjWfhkHUeDbfu1p8TdS6hsYi3lZgQFbVHPTI+xPWND4VkfaFDbfKdQq5FPdIkfRzp026VDJFxXeiuNp+tlHi5IbKdT1rSezxd7DYN8wF9PeI5YKPeK0P7siTIyI0RIKudiCcwWiO+gj0rYYfDpc4dbd1ze5Zrkag5Vd8+UrqGyMxBGzBT0pYVbHkdF/wAOxL3Qbd9vtEgOgGVW/ZubkurQSNY3BErVkzk1k+K3msshuXCHTMbOICyLqHKHtXlUb6oSRAMSMpq44Pxi3illeVwAXQ7qfA/eE9R9Nq7I0c0r7J139rt/Tr/f0qNcw6G5JHxLupKmVM/EpBMhv5amVGu8o/Cc4/CPiA9Cw9RWmZQvdMvwufJ+YfPRvrTEvOP1iR+8nOp9IzD5epqUaBoFZwTFW2MB1ntIn5b0MKPs0/Co+QFdnQEQQCOx1FRbdjTlZlIZxpBEB2AGVgRt2E+NFsNEmlXFjcHRX8pQ+gMg/MU1L7Ew1t18eVh/KSfmKfIKO9Kuf6Snf6H+1KjkgpmLUEatE9FAP1M60eH4pkeZ5gwYeY6eRGlOImo95Y1FcTtbOns9Gw14XEVxswB/9em1dazfsxj5+yJ3kr+IfEPz+daOulO0RapipUqFMyOpTTaVABNCkaQaKAOd62rqVdQyncESD6VgfaHDrbzBCSA0DMS0KoCxJ1IGvyjpXoDVlfbFQMhjeQY8dCT9PlUfI+NlcPyowjNAr0T2MIbCAHUZnEeBM/nXnD7mehPoK2HsVjyilH+B3Kqf2LmVSFY9nHw+KEbsKh48vyLZlaK3j94hksEybKtb7mAxyEnubfuyfEmqrB425ZuLctnnG2kzOhUjqD2q/wD/APQsAwe3fTTNyP8AiGqH5Zh/CKzWH4Zde2LqsQWLhMp1z2lDlTpuUD5Y6oO9XlJp0Tik1Z6xgcV7xTmUo6mHQwSreY+JTuG6j1A7XB1G41Hj4etZfB8X9/hkxaj7dFIZVA58p5rZnYH4h1Eg7TOjweKW9bW4h5WHXQggwVYdGBBBHQg1VSTJSi0HDMCsDpoPLdf5SPWa61Dv3VtOsmA5I/qxPkDJ/jNdMNi7dzNkacpg6EQfWmmujLi+zua52fvDsx+vN/qp9R7d9feMmYZvijuoAQkd4IinYUSKFKlTsKFSpUqVhRiqZd2pUqhIsuwcLchlIMQyH+YV6LSpU8fQpBoUqVbJipUqVMBUDSpUACsx7aDkFGlUs/xZTD8jz3Ebt/vpR4PjrgLWs3I+fMpAIJXNlOo0IyiCNdKVKuXF2dE+i74vj7l3AWTcbMfeJqQJ+C54VN9nx/8ABU9VxtkqexN2yhj+FmHrSpV1Pv8Aol/0j8RQWLd/3XJGKYDLIge9dY+UDyA7CqLBcRvKWi9cEwxh2EsYk6GlSqb7KLosLfErxIm9cOjbux3Vu5o4biN23OS4yyJMdTETSpVmxElePYmP1p37L/apnAeJ3bl9czzpcXZRpGaNB3E0qVVTMS6ZtBSpUquRG0qVKsgf/9k=" alt="images" class="img-fluid" /></a>
                </div>
                <div class="category__tittle">
                    <h6><a href="#">اخرى</a></h6>
                    <span>+100</span>
                </div>
            </div>
            
        </div>
    </div>
    
</section>

<!--  Popular Courses Starts Here -->
<section class="section section--bg-offwhite-three featured-popular-courses main-popular-course">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <div class="featured-popular-courses-heading  align-content-center ">
                    <div class="main-heading">
                        <h3 class="font-title--md">مواصفات القسم  </h3>
                    </div>
                    <div class="nav-button featured-popular-courses-tabs">
                        <ul class="nav nav-pills mb-3" id="pills-tab" role="tablist">
                            <li class="nav-item" role="presentation">
                                <button class="nav-link active ps-0" id="pills-all-tab" data-bs-toggle="pill" data-bs-target="#pills-all" type="button" role="tab" aria-controls="pills-all" aria-selected="true">
                                        الكل
                                    </button>
                            </li>
                            <li class="nav-item" role="presentation">
                                <button class="nav-link" id="pills-design-tab" data-bs-toggle="pill" data-bs-target="#pills-design" type="button" role="tab" aria-controls="pills-design" aria-selected="false">
                                        معدات سلامة عامة
                                    </button>
                            </li>
                            <li class="nav-item" role="presentation">
                                <button class="nav-link" id="pills-dev-tab" data-bs-toggle="pill" data-bs-target="#pills-dev" type="button" role="tab" aria-controls="pills-dev" aria-selected="false">
                                        محولات
                                    </button>
                            </li>
                            <li class="nav-item" role="presentation">
                                <button class="nav-link" id="pills-bus-tab" data-bs-toggle="pill" data-bs-target="#pills-bus" type="button" role="tab" aria-controls="pills-bus" aria-selected="false">
                                        كوابل
                                    </button>
                            </li>
                            <li class="nav-item" role="presentation" style="margin-left: 3%; margin-left: 3%; ">
                                <button class="nav-link " id="pills-its-tab" data-bs-toggle="pill" data-bs-target="#pills-its" type="button" role="tab" aria-controls="pills-its" aria-selected="false">
                                    اخرى
                                </button>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="tab-content" id="pills-tabContent">
                <div class="tab-pane fade show active" id="pills-all" role="tabpanel" aria-labelledby="pills-all-tab">
                    <div class="row">

                    </div>
                    <div class="row">
                        <div class="col-lg-12 text-center">
                            <a href="course-search.html" class="button button-lg button--primary">عرض كل المواصفات</a>
                        </div>
                    </div>
                </div>
                <div class="tab-pane fade" id="pills-design" role="tabpanel" aria-labelledby="pills-design-tab">
                    <div class="row">

                    </div>
                    <div class="row">
                        <div class="col-lg-12 text-center">
                            <a href="course-search.html" class="button button-lg button--primary">عرض كل المواصفات</a>
                        </div>
                    </div>
                </div>
                <div class="tab-pane fade" id="pills-dev" role="tabpanel" aria-labelledby="pills-dev-tab">
                    <div class="row">

                    </div>
                    <div class="row">
                        <div class="col-lg-12 text-center">
                            <a href="course-search.html" class="button button-lg button--primary">عرض كل المواصفات</a>
                        </div>
                    </div>
                </div>
                <div class="tab-pane fade" id="pills-bus" role="tabpanel" aria-labelledby="pills-bus-tab">
                    <div class="row">
                    
                        <div class="col-xl-4 col-md-6">
                            <div class="contentCard contentCard--course">
                                <div class="contentCard-top">
                                    <a href="course-details.html"><img src="https://www.emworks.com/blog/storage/uploads/2017/06/cross-section-of-a-3-phase-submarine-cable.jpg" alt="images" class="img-fluid" /></a>
                                </div>
                                <div class="contentCard-bottom">
                                    <h5>
                                        <a href="course-details.html" class="font-title--card">مواصفة كوابل</a>
                                    </h5>
                                    <div class="contentCard-info d-flex align-items-center justify-content-between">
                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-12 text-center">
                            <a href="course-search.html" class="button button-lg button--primary">عرض كل المواصفات</a>
                        </div>
                    </div>
                </div>
                <div class="tab-pane fade" id="pills-its" role="tabpanel" aria-labelledby="pills-its-tab">
                    <div class="row">
                        <!-- <div class="col-xl-4 col-md-6">
                            <div class="contentCard contentCard--course">
                                <div class="contentCard-top">
                                    <a href="course-details.html"><img src="dist/images/courses/demo-img-01.png" alt="images" class="img-fluid" /></a>
                                </div>
                                <div class="contentCard-bottom">
                                    <h5>
                                        <a href="course-details.html" class="font-title--card">Chicago International Conference on Education</a>
                                    </h5>
                                    <!-- <div class="contentCard-info d-flex align-items-center justify-content-between">
                                        <a href="instructor-profile.html" class="contentCard-user d-flex align-items-center">
                                            <img src="dist/images/courses/7.png" alt="client-image" class="rounded-circle" />
                                            <p class="font-para--md">Brandon Dias</p>
                                        </a>
                                        <div class="price">
                                            <span>$12</span>
                                            <del>$95</del>
                                        </div>
                                    </div> 
                                    <div class="contentCard-more">
                                        <div class="d-flex align-items-center">
                                            <div class="icon">
                                                <img src="dist/images/icon/star.png" alt="star" />
                                            </div>
                                            <span>4.5</span>
                                        </div>
                                        <div class="eye d-flex align-items-center">
                                            <div class="icon">
                                                <img src="dist/images/icon/eye.png" alt="eye" />
                                            </div>
                                            <span>24,517</span>
                                        </div>
                                        <div class="book d-flex align-items-center">
                                            <div class="icon">
                                                <img src="dist/images/icon/book.png" alt="location" />
                                            </div>
                                            <span>37 Lesson</span>
                                        </div>
                                        <div class="clock d-flex align-items-center">
                                            <div class="icon">
                                                <img src="dist/images/icon/Clock.png" alt="clock" />
                                            </div>
                                            <span>3 Hours</span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div> -->
                    </div>
                    <div class="row">
                        <div class="col-lg-12 text-center">
                            <a href="course-search.html" class="button button-lg button--primary">عرض كل المواصفات</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="featured-popular-courses-shape">
        <img src="dist/images/shape/dots/dots-img-12.png" alt="Shape" class="img-fluid dot-06" />
        <img src="dist/images/shape/triangel.png" alt="Shape" class="img-fluid dot-07" />
    </div>
</section>


</x-guest-layout>


