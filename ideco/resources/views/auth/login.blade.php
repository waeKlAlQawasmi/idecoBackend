<x-guest-layout>

    <section class="section signup-area signin-area">
        <div class="container">
            <div class="row align-items-center" style="
            direction: ltr;
        ">
                <div class="col-xl-5 order-2 order-xl-0">
                    <div class="signup-area-textwrapper">
                        <h2 class="font-title--md mb-0">تسجيل الدخول</h2>
                        <x-validation-errors class="mb-4 alert alert-danger " />

                        @if (session('status'))
                            <div class="mb-4 font-medium text-sm text-green-600 alert alert-danger">
                                {{ session('status') }}
                            </div>
                        @endif
                        {{-- <p class="mt-2 mb-lg-4 mb-3">Don't have account? <a href="signup.html" class="text-black-50">Sign up</a></p> --}}
                        @if (session('status'))
                            <div class="mb-4 font-medium text-sm text-green-600">
                                {{ session('status') }}
                            </div>
                        @endif
                        <form  method="POST" action="{{ route('login') }}">
                            @csrf
                            <div class="form-element {{-- success --}}">
                                <div class="form-alert">
                                    {{-- <label for="name">الايميل</label> --}}
                                </div>
                                <div class="form-alert-input">
                                    <input type="email" placeholder="البريد الالكتروني" name="email" value="{{old('email')}}" />
                                    <div class="form-alert-icon">
                                        {{-- <svg
                                            xmlns="http://www.w3.org/2000/svg"
                                            width="24"
                                            height="24"
                                            viewBox="0 0 24 24"
                                            fill="none"
                                            stroke="currentColor"
                                            stroke-width="2"
                                            stroke-linecap="round"
                                            stroke-linejoin="round"
                                            class="feather feather-check"
                                        >
                                            <polyline points="20 6 9 17 4 12"></polyline>
                                        </svg> --}}
                                    </div>
                                </div>
                            </div>
                            <div class="form-element active">
                                {{-- <div class="d-flex justify-content-between">
                                    <label for="confirm-password">Password</label>
                                    <a href="forget-password.html" class="text-primary fs-6">Forget Password</a>
                                </div> --}}

                                <div class="form-alert-input">
                                    <input type="password"  id="password"  name="password" placeholder="الرقم السري" value="{{old('password')}}" />
                                    <div class="form-alert-icon" onclick="showPassword('confirm-password',this);">
                                        <svg
                                            xmlns="http://www.w3.org/2000/svg"
                                            width="24"
                                            height="24"
                                            viewBox="0 0 24 24"
                                            fill="none"
                                            stroke="currentColor"
                                            stroke-width="2"
                                            stroke-linecap="round"
                                            stroke-linejoin="round"
                                            class="feather feather-eye"
                                        >
                                            <path d="M1 12s4-8 11-8 11 8 11 8-4 8-11 8-11-8-11-8z"></path>
                                            <circle cx="12" cy="12" r="3"></circle>
                                        </svg>
                                    </div>
                                </div>
                            </div>
                            <div class="form-element d-flex align-items-center terms">
                                <input class="checkbox-primary me-1" type="checkbox"  id="remember_me" name="remember" />
                                <label for="agree" class="text-secondary mb-0 fs-6">تذكر دخولي</label>
                            </div>
                            <div class="form-element">
                                <button type="submit" class="button button-lg button--primary w-100">دخول</button>
                            </div>

                        </form>
                    </div>
                </div>
                <div class="col-xl-7 order-1 order-xl-0">
                    <div class="signup-area-image">
                        <img src="https://media.licdn.com/dms/image/C4D12AQEKCZNTWFk43Q/article-cover_image-shrink_720_1280/0/1578482620102?e=2147483647&v=beta&t=Ps9J1eI5hG6VehEWMsQY3TsmkkF-nWs6XrnvluEHTf8" alt="Illustration Image" class="img-fluid" />
                    </div>
                </div>
            </div>
        </div>
    </section>

    {{-- <script src="{{ asset('src/js/jquery.min.js')}}"></script>
        <script src="{{ asset('src/js/bootstrap.bundle.min.js')}}"></script>
        <script src="{{ asset('src/scss/vendors/plugin/js/isotope.pkgd.min.js')}}"></script>
        <script src="{{ asset('src/scss/vendors/plugin/js/jquery.magnific-popup.min.js')}}"></script>
        <script src="{{ asset('src/scss/vendors/plugin/js/slick.min.js')}}"></script>
        <script src="{{ asset('src/scss/vendors/plugin/js/jquery.nice-select.min.js')}}"></script>
        <script src="{{ asset('src/js/app.js')}}"></script>
        <script src="{{ asset('dist/main.js')}}"></script> --}}

    {{-- <x-authentication-card>
        <x-slot name="logo">
            <x-authentication-card-logo />
        </x-slot>

        <x-validation-errors class="mb-4" />

        @if (session('status'))
            <div class="mb-4 font-medium text-sm text-green-600">
                {{ session('status') }}
            </div>
        @endif

        <form method="POST" action="{{ route('login') }}">
            @csrf

            <div>
                <x-label for="email" value="{{ __('Email') }}" />
                <x-input id="email" class="block mt-1 w-full" type="email" name="email" :value="old('email')" required autofocus autocomplete="username" />
            </div>

            <div class="mt-4">
                <x-label for="password" value="{{ __('Password') }}" />
                <x-input id="password" class="block mt-1 w-full" type="password" name="password" required autocomplete="current-password" />
            </div>

            <div class="block mt-4">
                <label for="remember_me" class="flex items-center">
                    <x-checkbox id="remember_me" name="remember" />
                    <span class="ml-2 text-sm text-gray-600">{{ __('Remember me') }}</span>
                </label>
            </div>

            <div class="flex items-center justify-end mt-4">
                @if (Route::has('password.request'))
                    <a class="underline text-sm text-gray-600 hover:text-gray-900 rounded-md focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500" href="{{ route('password.request') }}">
                        {{ __('Forgot your password?') }}
                    </a>
                @endif

                <x-button class="ml-4">
                    {{ __('Log in') }}
                </x-button>
            </div>
        </form>
    </x-authentication-card> --}}
</x-guest-layout>
