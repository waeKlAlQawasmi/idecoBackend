<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            {{ __('Dashboard') }}
        </h2>
    </x-slot>
    <link rel="stylesheet" href="https://cdn.datatables.net/1.13.6/css/jquery.dataTables.min.css">
    <x-nav-link href="{{ route('item.create') }}" :active="request()->routeIs('dashboard')">
        {{ __('create') }}
    </x-nav-link>

    <div class="py-12">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
            <div class="bg-white overflow-hidden shadow-xl sm:rounded-lg" style="padding: 15px">
                <table id="example" class="display" style="width:100%">
                    <thead>
                        <tr>
                            <th>اسم المادة</th>
                            <th>رقم المادة</th>
                            <th>رقم المواصفة</th>
                            <th> التصنيف</th>
                             <th> حذف</th>
                             <th> تعديل</th>

                            
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($items as $item)
                        <tr>
                            <td><a href="{{route('item.show',$item) }}"> {{ $item->name}}</a></td>
                            <td>{{ $item->number}}</td>
                            <td>{{ $item->spesfication_number}}</td>
                            <td>{{ $item->category->name}}</td>
                            <td> <form action="{{  route('item.destroy',$item) }}" method="post">
                                @csrf
                                @method("delete")
                                <button type="submit" class ="btn btn-danger  btn-danger ">حذف</button>
                            </form>
                               
                              </td>
                              <td><a href="{{ route('item.edit',$item)}}">تعديل</td>
                            
                        </tr>
                            
                        @endforeach
                       
                     
                    </tbody>
                    <tfoot>
                        <tr>
                            <th>اسم المادة</th>
                            <th>رقم المادة</th>
                            <th>رقم المواصفة</th>
                            <th> التصنيف</th>
                            <th> حذف</th>
                            <th> تعديل</th>
                            
                        </tr>
                    </tfoot>
                </table>
            </div>
        </div>
    </div>

    <script src="https://code.jquery.com/jquery-3.7.0.js"></script>
    <script src="https://cdn.datatables.net/1.13.6/js/jquery.dataTables.min.js"></script>
<script>
  new DataTable('#example');
</script>
</x-app-layout>
