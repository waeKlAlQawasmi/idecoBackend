<x-app-layout>
    <x-slot name="header">
        {{-- <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            {{ __('ادارة قسم المواصفات') }}
        </h2> --}}
    </x-slot>
    <link rel="stylesheet" href="{{ asset('dist/main.css') }}">

    <div class="py-12">
   
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
            <div class="bg-white overflow-hidden shadow-xl sm:rounded-lg">
                <div class="col-lg-12 form-area">
                    <form method="POST" action="{{ route('item.store') }}" enctype="multipart/form-data"  >
                        @csrf
                        <div class="row g-3">
                            <div class="col-lg-6">
                                <label for="name">اسم المادة</label>
                                <input type="text" name="name" class="form-control form-control--focused" placeholder="Type here..." id="name" />
                            </div>
                            <div class="col-lg-6">
                                <label for="category">التصنيف</label>
                                <select name="category_id" class="form-control" id="category" style=" padding: 15px;">
                                    @foreach ($categories as $item)
                                        <option value="{{$item->id}}">{{$item->name}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="row my-lg-2 my-2">
                            <div class="col-6">
                                <label for="subject">صورة المادة</label>
                                <input type="file" name="img" accept="image/*" id="subject" class="form-control" placeholder="Type here..." />
                            </div> <div class="col-3">
                                <label for="subject"> رقم المادة</label>
                                <input type="text" name="number" id="subject" class="form-control" placeholder="Type here..." />
                            </div>
                            <div class="col-3">
                                <label for="subject"> رقم المواصفة</label>
                                <input type="text" name="spesfication_number" id="subject" class="form-control" placeholder="Type here..." />
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-12">
                                <label for="message">وصف المادة</label>
                                <textarea name="description" id="message" placeholder="Type here..." class="form-control"></textarea>
                            </div>
                        </div>
                        <div class="row">
                            <div class="text-end">
                                <button type="submit" class="button button-lg button--primary fw-normal">حفظ </button>
                            </div>
                        </div>
                    </form>
                    <div class="form-area-shape">
                        <img src="dist/images/shape/circle3.png" alt="Shape" class="img-fluid shape-01" />
                        <img src="dist/images/shape/circle5.png" alt="Shape" class="img-fluid shape-02" />
                    </div>
                </div>
            </div>
        </div>
    </div>
</x-app-layout>


