<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            {{ __('Dashboard') }}
        </h2>
    </x-slot>
    <link rel="stylesheet" href="https://cdn.datatables.net/1.13.6/css/jquery.dataTables.min.css">


    <div class="py-12">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
            <div class="bg-white overflow-hidden shadow-xl sm:rounded-lg" style="padding: 15px">
                <table id="example" class="display" style="width:100%">
                    <thead>
                        <tr>
                            <th>اسم التصميف</th>
                            <th>حذف</th>
                            
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($categories as $item)
                        <tr>
                            <td>{{ $item->name}}</td>
                            <td> <form action="{{  route('category.destroy',$item) }}" method="post">
                                @csrf
                                @method("delete")
                                <button type="submit" class ="btn btn-danger  btn-danger ">حذف</button>
                            </form>
                               
                              </td>
                            
                        </tr>
                            
                        @endforeach
                       
                     
                    </tbody>
                    <tfoot>
                        <tr>
                            <th>Name</th>
                            <th>delete</th>
                            
                        </tr>
                    </tfoot>
                </table>
            </div>
        </div>
    </div>

    <script src="https://code.jquery.com/jquery-3.7.0.js"></script>
    <script src="https://cdn.datatables.net/1.13.6/js/jquery.dataTables.min.js"></script>
<script>
  new DataTable('#example');
</script>
</x-app-layout>
